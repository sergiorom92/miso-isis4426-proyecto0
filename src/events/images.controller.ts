import { Param, Controller, Get, HttpStatus, Res } from '@nestjs/common/';

@Controller('image')
export class ImagesController {
  constructor() {}

  @Get(':imageName')
  getImage(@Param('imageName') image, @Res() res) {
    const response = res.sendFile(image, { root: './uploads' });
    return {
      status: HttpStatus.OK,
      data: response,
    };
  }
}
