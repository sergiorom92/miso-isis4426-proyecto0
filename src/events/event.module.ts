import { Module } from '@nestjs/common';
import { EventsController } from './events.controller';
import { EventsService } from './events.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventRepository } from './event.repository';
import { AuthModule } from '../auth/auth.module';
import { ImagesController } from './images.controller';

@Module({
  imports: [TypeOrmModule.forFeature([EventRepository]), AuthModule],
  controllers: [EventsController, ImagesController],
  providers: [EventsService],
})
export class EventsModule {}
