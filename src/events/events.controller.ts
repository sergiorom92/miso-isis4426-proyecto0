import {
  Body,
  Param,
  Controller,
  Delete,
  Get,
  Post,
  Put,
  UseInterceptors,
  UseGuards,
  UploadedFile,
  HttpStatus,
  Res,
} from '@nestjs/common/';
import { Event } from './event.entity';
import { EventsService } from './events.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../auth/get-user.decorator';
import { User } from '../auth/user.entity';
import { EventDto } from './dto/event.dto';
import { diskStorage } from 'multer';
import { editFileName, imageFileFilter } from '../utils/file-upload';

@Controller('events')
@UseGuards(AuthGuard())
export class EventsController {
  constructor(private eventsService: EventsService) {}

  @Get('image/:imageName')
  getImage(@Param('imageName') image, @Res() res) {
    const response = res.sendFile(image, { root: './uploads' });
    return {
      status: HttpStatus.OK,
      data: response,
    };
  }

  @Get()
  getEvents(@GetUser() user: User): Promise<Event[]> {
    return this.eventsService.getAllEvents(user);
  }

  @Get(':id')
  getEventById(@Param('id') id: string, @GetUser() user: User): Promise<Event> {
    return this.eventsService.getEventById(id, user);
  }

  @Post()
  @UseInterceptors(
    FileInterceptor('thumbnail', {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  createEvent(
    @UploadedFile() file,
    @Body() event: EventDto,
    @GetUser() user: User,
  ) {
    event.thumbnail = file ? file.filename : '';
    return this.eventsService.createEvent(event, user);
  }

  @Put(':id')
  @UseInterceptors(
    FileInterceptor('thumbnail', {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  updateEvent(
    @Param('id') id,
    @Body() event: EventDto,
    @GetUser() user: User,
    @UploadedFile() file,
  ) {
    if (file) {
      event.thumbnail = file ? file.filename : '';
    }
    return this.eventsService.updateEvent(id, event, user);
  }

  @Delete(':id')
  deleteEvent(@Param('id') id, @GetUser() user: User) {
    return this.eventsService.deleteEvent(id, user);
  }
}
