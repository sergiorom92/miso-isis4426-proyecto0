FROM node:10-alpine as build-step

WORKDIR /app

ADD . /app

RUN npm config set registry http://registry.npmjs.org
RUN npm install

EXPOSE 3000

CMD ["npm", "run", "start"]
